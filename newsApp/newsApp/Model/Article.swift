//
//  Article.swift
//  newsApp
//
//  Created by Sharra on 2021-05-22.
//

import Foundation

struct ArticleList : Decodable{
    let articles : [Article]
}

struct Article : Decodable{
    var title : String?
    var description : String?

    
    
}
