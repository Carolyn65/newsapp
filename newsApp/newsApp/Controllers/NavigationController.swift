//
//  NavigationController.swift
//  newsApp
//
//  Created by Sharra on 2021-05-22.
//

import Foundation
import UIKit

//Overide the status bar style so that it returns the correct colour for the viewControllser
class MyRootNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override var childForStatusBarStyle: UIViewController? {
        return nil
    }
}
