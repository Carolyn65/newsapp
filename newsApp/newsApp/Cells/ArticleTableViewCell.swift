//
//  ArticleTableViewCell.swift
//  newsApp
//
//  Created by Sharra on 2021-05-22.
//

import Foundation
import UIKit

class ArticleTableViewCell : UITableViewCell{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}
